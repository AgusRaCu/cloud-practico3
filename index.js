var AWS = require('aws-sdk');
exports.handler = (event, context, callback) => {
    var dynamodb = new AWS.DynamoDB({
        apiVersion: '2012-08-10',
        endpoint: 'http://dynamodb:8000',
        region: 'us-west-2',
        credentials: {
            accessKeyId: '2345',
            secretAccessKey: '2345'
        }
    });
    var docClient = new AWS.DynamoDB.DocumentClient({
        apiVersion: '2012-08-10',
        service: dynamodb
    });
    let id = (event.pathParameters || {}).idEnvio || false;
    let envio = JSON.parse(event.body) || false;
    switch (event.httpMethod) {
        case "GET":
            if (event.path.includes('/envios/pendientes')) {
                var params = {
                    TableName: 'Envio',
                    IndexName: 'EnviosPendientesIndex'
                };
                dynamodb.scan(params, function (err, data) {
                    if (err) {
                        callback(err, null);
                    }
                    else {
                        callback(null, {
                            body: JSON.stringify({
                                message: "Envios Pendientes: ",
                                data: data.Items})
                        });
                    }
                });
                break;
            }
            else if (id) {
                var params = {
                    TableName: 'Envio',
                    Key: {
                        id: id
                    }
                };
                docClient.get(params, function (err, data) {
                    if (err) {
                        console.log("ERROR", err);
                        callback(err, null);
                    }
                    else {
                        console.log(`success: returned ${data.Item}`);
                        callback(null, {
                            body: JSON.stringify({
                                message: "Envio numero: "+id,
                                data: data.Item})
                        });
                    }
                });
                break;
            }
            break;
        case "POST":
            if (event.path.includes('/entregado') && id) {
                var params = {
                    TableName: 'Envio',
                    Key: {
                        id: id
                    },
                    UpdateExpression: 'remove pendiente',
                    ReturnValues: 'ALL_NEW'
                };
                docClient.update(params, function (err, data) {
                    if (err) {
                        console.log(`error`, err);
                        callback(err, null);
                    }
                    else {
                        callback(null, {
                            body: JSON.stringify({
                                message: "Envio Entregado: ",
                                data: data.Attributes
                            })
                        });
                    }
                });
            }
            if (event.path.includes('/envios') ) {
                var params = {
                    TableName: 'Envio',
                    Item: envio
                };
                docClient.put(params, function (err, data) {
                    if (err) {
                        console.log(`error`, err);
                        callback(err, null);
                    }
                    else {
                        callback(null, {
                            body: JSON.stringify({
                                message: "Envio creado",
                                data: envio
                            }) 
                        });
                    }
                });
            }
            break;
        default:
            console.log("Metodo no soportado (" + event.httpMethod+ ")");
    }
    
}